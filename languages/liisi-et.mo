��          �       �      �     �  )   �          '     ;     H     U     t     �     �     �     �     �     �     �          "     7  
   S     ^  ,   e  �   �  (   &     O  >   i     �    �     �  "   �          $     5     G     L     j  
   |     �     �     �     �     �     �             0   "     S     `  0   g  �   �     	     5	  6   K	  "   �	   Annual interest rate (%) Delay time to redirect to payment gateway Delete current picture Display Description Display Name Enable Liisi Enable product monthly payment Enable test mode Enable/Disable Fill Liisi credit application Image for payment logo Maximum %d seconds Order number %d Payment Settings Payment period in months Private key Private key password Product monthly payment url Public key SND ID Set payment period in months, within 3 to 60 Sorry, <strong>WooCommerce %s</strong> requires WooCommerce to be installed and activated first. Please install <a href="%s">WooCommerce</a> first. The total number of periods is %d months Upload Liisi logo picture Uploaded file is not a valid image! Only PNG files are allowed Uploaded successfully! Project-Id-Version: liisi
POT-Creation-Date: 2019-06-21 15:18+0000
PO-Revision-Date: 2019-06-21 15:20+0000
Last-Translator: Andrei Ushakov andrei@ushakov.eu
Language-Team: Estonian
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: woocommerce-payment-gateway-liisi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.2.2; wp-5.2.2 Aasta intressimäär (%) Makseviisile suunamise viivitusaeg Kustutada praegune pilt Mooduli selgitus Maksemooduli nimi Luba Luba Liisi kuumakse pakkumine Luba testkeskkond Luba/Keela Maksa Liisi järelmaksuga Liisi logo pilt Maksimaalne %d sekundit Tellimuse number %d Seaded Sisesta järelmaksu periood Privaatvõti Privaatvõtme parool Järelmaksu kampaania kirjelduse lehekülg (url) Avalik võti SND ID Järelmaksu perioodiks saad valida 3 – 60 kuud Vabandame, <strong>WooCommerce %s</strong>peab olema paigaldatud ja aktiveeritud. Palun paigaldage <a href="%s">WooCommerce</a>. järelmaksu periood %d kuud Liisi logo tõmbamine Fail on vales formaadis. Lubatud on ainult PNG-failid! Fail edukalt saidile üles laetud. 