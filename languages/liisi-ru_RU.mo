��                �      �     �       )        D     [     o     |     �     �     �     �     �     �          "     2     C     \     h     }  
   �     �  9   �  ,   �  (        ;  >   U     �  c  �  N        ^  [   j  4   �  )   �  )   %     O  T   n  ,   �  %   �  P   	  -   g	     �	  &   �	     �	  4   �	  1   
     J
  ,   f
  E   �
     �
     �
  n   �
  E   i  O   �  '   �  �   '  )   �   Annual interest rate (%) Cheatin&#8217; huh? Delay time to redirect to payment gateway Delete current picture Display Description Display Name Enable Liisi Enable product monthly payment Enable test mode Enable/Disable Fill Liisi credit application Image for payment logo Kuumakse al. %1$s Maximum %d seconds Order number %d Payment Settings Payment period in months Private key Private key password Product monthly payment url Public key SND ID Set interest, that number can not more than 100 or less 0 Set payment period in months, within 3 to 60 The total number of periods is %d months Upload Liisi logo picture Uploaded file is not a valid image! Only PNG files are allowed Uploaded successfully! Project-Id-Version: liisi
POT-Creation-Date: 2019-06-21 15:21+0000
PO-Revision-Date: 2019-06-21 15:23+0000
Last-Translator: Andrei Ushakov andrei@ushakov.eu
Language-Team: Русский
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: woocommerce-payment-gateway-liisi
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.2.2; wp-5.2.2 Годовая процентная ставка при рассрочке (%) Нечто! Время задержки перенаправления на платежный шлюз Удалить текущее изображение Отображаемое описание Отображаемое название Использовать Liisi Включить предложение Liisi на странице продукта Включить тестовый режим Разрешить/Запретить Заполнить ходатайство о рассрочке платежей Изображение логотипа Liisi от %1$s/месяц Максимально %d секунд Заказ № %d Настройка платежной системы Период рассрочки в месяцах Приватный ключ Пароль приватного ключа Ссылка на информационную страницу (url) Открытый ключ SND ID Назначить интресс, это число должно быть в пределах от 0 до 100 Период можно выбрать от 3 до 60 месяцев. Общий период платежей составляет %d месяцев Загрузить логотип Liisi Загруженный файл не является допустимым изображением! Разрешены только файлы в формате PNG! Файл успешно загружен! 