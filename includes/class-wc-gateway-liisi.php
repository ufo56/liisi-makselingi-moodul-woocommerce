<?php
if (!defined('ABSPATH'))
    exit;

class WC_Gateway_Liisi extends WC_Payment_Gateway {

    public $form_fields = array();
    public $maxtime;
    public $liisi_logo_pic;
    public $liisi_intress;
    public $liisi_periood;
    public $liisi_periood_text;
    public $liisi_enabled_on_product_page;
    public $liisi_product_url;
    public $liisi_product_page_text;
    public $default_logo_pic;
    public $default_checkout_logo_pic;
    public $checkout_logo_pic;
    public $no_logo_pic;
    public $min_payment;

    public function __construct() {

        $this->id = 'liisi_banklink';
        $this->has_fields = false;
        $this->credit_fields = false;
        $this->method_title = __('Liisi banklink', 'liisi');
        $this->method_description = __('Fill Liisi credit application', 'liisi');
        $this->default_liisi_product_page_text = __('Kuumakse al. %1$s', 'liisi');
        $this->liisi_product_page_text = $this->get_option('calc_product_page_text')
            ?: $this->default_liisi_product_page_text;
        //$this->default_logo_pic = plugins_url( '/assets/images/liisi_bk.png', dirname( __FILE__ ));
        $this->default_logo_pic = '';
        $this->default_checkout_logo_pic = plugins_url( '/assets/images/liisi.png', dirname( __FILE__ ));
        $this->no_logo_pic = plugins_url('/assets/images/no_logo.png', dirname(__FILE__));
        ;
        if ($this->get_option('display_name')) {
            $this->title = $this->get_option('display_name');
        } else {
            $this->title = $this->method_title;
        }

        if ($this->get_option('display_description')) {
            $this->description = $this->get_option('display_description');
        } else {
            $this->description = $this->method_description;
        }
        $this->liisi_periood = $this->get_option('liisi_periood'); // 60 järelmaksu periood _ kuud
        $this->min_payment = 7; // Minimaalne järelmaksu igakuise osamakse suurus
        $this->maxtime = 200;
        $this->liisi_periood_text = __('The total number of periods is %d months', 'liisi');

        $setmaxtime = intval($this->get_option('delay_time_redirect_to_payment_gateway'));

        $this->enabled = $this->get_option('enabled');
        $this->test_mode = $this->get_option('test_mode');
        $this->snd = $this->get_option('snd');
        $this->private_key = $this->get_option('private_key');
        $this->public_key = $this->get_option('public_key');
        $this->private_key_pass = $this->get_option('private_key_pass');
        $this->liisi_logo_pic = $this->get_option('product_monthly_picture');
        $this->liisi_intress = (floor(floatval($this->get_option('product_monthly_intress'))));
        $this->liisi_product_url = $this->get_option('product_monthly_payment_url');

        If ($this->liisi_logo_pic == '') {
            $this->liisi_logo_pic = $this->default_logo_pic;
        }

        $this->checkout_logo_pic = $this->get_option('checkout_payment_logo');
        if (empty($this->checkout_logo_pic)) {
            $this->checkout_logo_pic = $this->default_checkout_logo_pic;
        }

        $this->icon = apply_filters('woocommerce_gateway_liisi_icon', $this->checkout_logo_pic);

        if ($this->get_option('product_monthly_payment')) {
            $this->liisi_enabled_on_product_page = $this->get_option('product_monthly_payment');
        } else
            $this->liisi_enabled_on_product_page = 'no';

        $this->init_form_fields();
        $this->init_settings();


        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('admin_enqueue_scripts', 'liisi_scripts_with_jquery');
        }

        //add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));
        add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));

        ##################### DEBUG UTILITY########################
        # https://wordpress.t.vptest.ee/wc-api/liisi-debug
        # add_action('woocommerce_api_liisi-debug', array($this, 'webhook_debug'));
        ###########################################################

        //css style for Liisi logo on front product page
        wp_enqueue_style('liisi_logo_front', plugins_url('/assets/css/liisifront.css', dirname(__FILE__)));
    }

    public function init_form_fields() {

        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable/Disable', 'liisi'),
                'label' => __('Enable Liisi', 'liisi'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ),
            'test_mode' => array(
                'title' => __('Enable test mode', 'liisi'),
                'label' => __('Enable test mode', 'liisi'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'yes'
            ),
            'display_name' => array(
                'title' => __('Display Name', 'liisi'),
                'type' => 'text',
                'description' => '',
                'default' => 'Liisi',
                'desc_tip' => false
            ),
            'display_description' => array(
                'title' => __('Display Description', 'liisi'),
                'type' => 'textarea',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'snd' => array(
                'title' => __('SND ID', 'liisi'),
                'type' => 'text',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'private_key' => array(
                'title' => __('Private key', 'liisi'),
                'type' => 'textarea',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'public_key' => array(
                'title' => __('Public key', 'liisi'),
                'type' => 'textarea',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'private_key_pass' => array(
                'title' => __('Private key password', 'liisi'),
                'type' => 'text',
                'description' => '',
                'default' => '',
                'desc_tip' => false
            ),
            'delay_time_redirect_to_payment_gateway' => array(
                'title' => __('Delay time to redirect to payment gateway', 'liisi'),
                'type' => 'number',
                'description' => sprintf(__('Maximum %d seconds', 'liisi'), $this->maxtime),
                'default' => '0',
                'desc_tip' => false
            ),
            'product_monthly_payment' => array(
                'title' => __('Enable product monthly payment', 'liisi'),
                'label' => __('Enable product monthly payment', 'liisi'),
                'type' => 'checkbox',
                'description' => sprintf($this->liisi_periood_text, $this->liisi_periood),
                'default' => 'no',
                'desc_tip' => true
            ),
            'product_monthly_intress' => array(
                'title' => __('Annual interest rate (%)', 'liisi'),
                'type' => 'decimal',
                'description' => __('Set interest, that number can not more than 100 or less 0', 'liisi'),
                'default' => '',
                'desc_tip' => true
            ),
            'liisi_periood' => array(
                'title' => __('Payment period in months', 'liisi'),
                'type' => 'number',
                'description' => __('Set payment period in months, within 3 to 60', 'liisi'),
                'default' => '60',
                'desc_tip' => false
            ),
            'product_monthly_payment_url' => array(
                'title' => __('Product monthly payment url', 'liisi'),
                'type' => 'url',
                'description' => 'For example "https://www.yourdomain.ee/liisi-jarelmaks/"',
                'default' => '',
                'desc_tip' => false
            ),
            'calc_product_page_text' => array(
                'title' => __('Product page calculator text', 'liisi'),
                'type' => 'text',
                'description' => __('Product page calculator text, keep placeholder %1$s for displaying monthly amount', 'liisi'),
                'default' => $this->default_liisi_product_page_text,
                'desc_tip' => false
            ),
            'product_monthly_picture' => array(
                'title' => __('Image for payment logo', 'liisi'),
                'type' => 'liisilogo',
                'description' => 'Set the image to use for product monthly payment',
                'default' => '',
                'custom_attributes' => array('onclick' => "location.href='http://'",),
                'class' => 'button button-primary',
                'desc_tip' => true
            ),
            'checkout_payment_logo' => array(
                'title' => __('Image for payment method at checkout page', 'liisi'),
                'type' => 'liisicheckoutlogo',
                'description' => 'Set the image to use for payment method',
                'default' => '',
                'custom_attributes' => array('onclick' => "location.href='http://'",),
                'class' => 'button button-primary',
                'desc_tip' => true
            ),
        );
    }

    public function generate_liisilogo_html($key, $data) {
    $field = $this->plugin_id . $this->id . '_' . $key;
    $defaults = array(
        'class' => 'button',
        'css' => '',
        'custom_attributes' => array(),
        'desc_tip' => false,
        'description' => '',
        'title' => '',
    );

    $data = wp_parse_args($data, $defaults);
    $widthimage = 200; // image width in px for Admin config panel
    $urlimage = $this->get_option('product_monthly_picture');
    if (!$urlimage)
        $urlimage = $this->default_logo_pic;
    $btn_text = __('Upload Liisi logo picture', 'liisi');
    if ($urlimage)
        $sizeimage = getimagesize($urlimage);
    if (isset($sizeimage) && isset($sizeimage[0]) && $sizeimage [0] < $widthimage) {
        $widthimage = $sizeimage [0];
    }

    //*** Liisi logo UPLOADING controller **
    if (!empty($_FILES['file']) && (!$_FILES['file']['error'])) {
        $file = $_FILES['file'];
        if ($file ['type'] == 'image/png') {
            $upload_dir = wp_upload_dir();
            $path = $upload_dir['basedir'] . '/liisilogo/';
            if (!is_dir($path)) {
                mkdir($path);
            }
            $file ['name'] = 'logo.png';
            $attachment_id = $this->upload_user_file($file, $path);
            if ($attachment_id) {
                $urlimage = $upload_dir['baseurl'] . '/liisilogo/' . $attachment_id;
                //echo "URL: ".$urlimage;
                $this->update_option('product_monthly_picture', $urlimage);
            }
        } else {
            echo ' <div id="message" class="notice notice-error is-dismissible"><p> ' . __('Uploaded file is not a valid image! Only PNG files are allowed', 'liisi') . '</p></div>';
        }
    }

    ob_start(); //  Print HTML form and field incl. JavaScript
    ?>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="<?php echo esc_attr($field); ?>"><?php echo wp_kses_post($data['title']); ?></label>
            <?php echo $this->get_tooltip_html($data); ?>
        </th>
        <td class="forminp">
            <img id="my_liisi" src="<?php if ($urlimage)
                echo $urlimage;
            else
                echo $this->no_logo_pic;
            ?> " style="width:<?php echo $widthimage; ?>px;"/>
            <fieldset>
                <legend class="screen-reader-text"><span><?php echo wp_kses_post($data['title']); ?></span></legend>
                <input id="SelectImage" type="file" name="file" accept="image/x-png" />
                <input id="DefImage" type="hidden" value="<?php if ($this->default_logo_pic)
                    echo esc_attr($this->default_logo_pic);
                else
                    echo esc_attr($this->no_logo_pic)
                ?>" />
                <button id="remove_image"type="button" title="<?php echo esc_attr(__('Delete current picture', 'liisi')); ?>" class="remove_image_button button">&times;</button>
                <?php echo $this->get_description_html($data); ?>
                <input id="imageNameL" type="hidden" name="<?php echo esc_attr($field); ?>" value="<?php echo esc_attr($urlimage); ?>" />
            </fieldset>
        </td>
    </tr>

    <?php
    // ******************* END OF HTML PART ********************
    return ob_get_clean();
}

    public function generate_liisicheckoutlogo_html($key, $data) {
        $field = $this->plugin_id . $this->id . '_' . $key;
        $defaults = array(
            'class' => 'button',
            'css' => '',
            'custom_attributes' => array(),
            'desc_tip' => false,
            'description' => '',
            'title' => '',
        );

        $data = wp_parse_args($data, $defaults);
        $widthimage = 200; // image width in px for Admin config panel

        $urlimage = $this->checkout_logo_pic;
        if ($urlimage)
            $sizeimage = getimagesize($urlimage);
        if (isset($sizeimage) && isset($sizeimage[0]) && $sizeimage [0] < $widthimage) {
            $widthimage = $sizeimage [0];
        }

        //*** Liisi logo UPLOADING controller **
        if (!empty($_FILES['file2']) && (!$_FILES['file2']['error'])) {
            $file = $_FILES['file2'];
            if ($file ['type'] == 'image/png') {
                $upload_dir = wp_upload_dir();
                $path = $upload_dir['basedir'] . '/liisilogo/';
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file ['name'] = 'logo2.png';
                $attachment_id = $this->upload_user_file($file, $path);
                if ($attachment_id) {
                    $urlimage = $upload_dir['baseurl'] . '/liisilogo/' . $attachment_id;
                    //echo "URL: ".$urlimage;
                    $this->update_option('checkout_payment_logo', $urlimage);
                }
            } else {
                echo ' <div id="message" class="notice notice-error is-dismissible"><p> ' . __('Uploaded file is not a valid image! Only PNG files are allowed', 'liisi') . '</p></div>';
            }
        }

        ob_start(); //  Print HTML form and field incl. JavaScript
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr($field); ?>"><?php echo wp_kses_post($data['title']); ?></label>
                <?php echo $this->get_tooltip_html($data); ?>
            </th>
            <td class="forminp">
                <img id="my_liisi2" src="<?php if ($urlimage)
                    echo $urlimage;
                else
                    echo $this->no_logo_pic;
                ?> " style="width:<?php echo $widthimage; ?>px;"/>
                <fieldset>
                    <legend class="screen-reader-text"><span><?php echo wp_kses_post($data['title']); ?></span></legend>
                    <input id="SelectImage2" type="file" name="file2" accept="image/x-png" />
                    <input id="DefImage2" type="hidden" value="<?php if ($this->default_checkout_logo_pic)
                        echo esc_attr($this->default_checkout_logo_pic);
                    else
                        echo esc_attr($this->no_logo_pic)
                    ?>" />
                    <button id="remove_image2" type="button" title="<?php echo esc_attr(__('Delete current picture', 'liisi')); ?>" class="remove_image_button button">&times;</button>
                    <?php echo $this->get_description_html($data); ?>
                    <input id="imageNameL2" type="hidden" name="<?php echo esc_attr($field); ?>" value="<?php echo esc_attr($urlimage); ?>" />
                </fieldset>
            </td>
        </tr>

        <?php
        // ******************* END OF HTML PART ********************
        return ob_get_clean();
    }

    public function upload_user_file($file, $path) {
        if (!empty($file)) {
            $result = "";
            $upload_dir = $path;
            $uploaded = move_uploaded_file($file['tmp_name'], $upload_dir . $file['name']);
            if ($uploaded) {
                $result = $file['name'];
                echo ' <div id="message" class="notice notice-success is-dismissible"><p> (' . $result . ') ' . __('Uploaded successfully!', 'liisi') . '</p></div>';
            } else {
                echo "some error in upload ";
                print_r($file['error']);
            }
            return $result;
        }
    }

    //*******************************
    //*** Liisi field validation  ***
    //*******************************
    public function validate_product_monthly_intress_field($key, $value) {
        if (isset($value) && ( $value < 0 || $value > 100 )) {
            WC_Admin_Settings::add_error(esc_html__('Looks like you made a mistake with the Intress rate field. Make sure it is not more than 100 or less 0', 'liisi'));
            $value = 0;
        }
        return $value;
    }

    public function validate_liisi_periood_field($key, $value) {
        if (isset($value) && ( $value < 3 || $value > 60 )) {
            WC_Admin_Settings::add_error(esc_html__('Looks like you made a mistake with the payment period. Make sure it is not more than 60 or less 3', 'liisi'));
            $value = 60;
        }
        return $value;
    }

    public function is_available() {
        if ($this->enabled == 'no') {
            return false;
        }

        if (!$this->private_key || !$this->snd || !$this->public_key) {
            return false;
        }
        return true;
    }

    private function getLiisiApi() {
        return new Liisi_Api(
                $this->test_mode,
                $this->snd,
                $this->private_key,
                $this->public_key,
                $this->private_key_pass
        );
    }

    private function getLiisiOrder(WC_Order $order) {
        $liisi_order = new Liisi_order($order->id, $order->order_total);
        $liisi_order->setCurrency($order->get_order_currency());
        $liisi_order->setMessage(sprintf(__('Order number %d', 'liisi'), $order->id));
        return $liisi_order;
    }

    public function thankyou_page($order_id) {
        $api = $this->getLiisiApi();

        if (!empty($_POST) && $api->vertifySign($_POST)) {
            $id_order = (int) (!empty($_POST['VK_STAMP']) ? $_POST['VK_STAMP'] : 0);
            $service = (int) (!empty($_POST['VK_SERVICE']) ? $_POST['VK_SERVICE'] : 0);
            $auto = (!empty($_POST['VK_AUTO']) ? $_POST['VK_AUTO'] : 'N');
            $order = new WC_Order((int) $order_id);

            switch ($service) {
                case Liisi_Api::RESPONSE_1111: //Success
                    $order->reduce_order_stock();
                    $order->update_status('processing', 'Liisi contract signed');
                    break;
                case Liisi_Api::RESPONSE_1911: //Cancel
                    $order->cancel_order(__('Liisi cancel'));
                    if ($auto == 'N') {
                        wp_safe_redirect($order->get_cancel_order_url());
                    }
                    break;
            }
        } else {
            wp_safe_redirect(home_url());
        }
    }

    public function receipt_page($order_id) {

        $api = $this->getLiisiApi();
        $order = new WC_Order((int) $order_id);
        $liisi_order = $this->getLiisiOrder($order);

        If ($this->get_option('delay_time_redirect_to_payment_gateway')) {
            $delay_time = intval($this->get_option('delay_time_redirect_to_payment_gateway'));
            if ($delay_time > $this->maxtime)
                $delay_time = $this->maxtime;
            if ($delay_time < 0)
                $delay_time = 0;
            $delay_time = $delay_time * 1000;
        } else {
            $delay_time = 0;
        }

        $form_fields = $api->getFormFields($liisi_order, $this->get_return_url($order));

        echo '<form method="POST" action="', $api->getUrl(), '">';
        foreach ($form_fields as $name => $value) {
            echo '<input type="hidden" name="', $name, '" value="', $value, '"/>';
        }
        echo '<p id="countdown_timer"> </p>';
        echo '<input id="banklink_btn" type="submit" value="', __('Submit'), '"/>';
        echo '</form>';
        // JavaScript - Set the counting down to press button
        // --------------------------------------------------
        echo '<script type="text/javascript">' . "\n";
        echo 'var countDownDate = new Date().getTime()+' . $delay_time . ';' . "\n";
        echo 'var x = setInterval(function() {' . "\n";
        echo 'var now = new Date().getTime();' . "\n";
        echo 'var distance = countDownDate - now;' . "\n";
        echo 'var days = Math.floor(distance / (1000 * 60 * 60 * 24));' . "\n";
        echo 'var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));' . "\n";
        echo 'var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));' . "\n";
        echo 'var seconds = Math.floor((distance % (1000 * 60)) / 1000);' . "\n";
        echo 'var countDownDate_html= " ... ";' . "\n";
        echo 'if (days) {countDownDate_html += " days: " + days;}' . "\n";
        echo 'if (hours) {countDownDate_html += " hours: " + hours;}' . "\n";
        echo 'if (minutes) {countDownDate_html += " minutes: " + minutes;}' . "\n";
        echo 'if (seconds) {countDownDate_html += " seconds: " + seconds;}' . "\n";
        echo 'document.getElementById("countdown_timer").innerHTML = countDownDate_html;' . "\n";
        echo 'if (distance < 0) {' . "\n";
        echo 'clearInterval(x);' . "\n";
        echo 'document.getElementById("countdown_timer").innerHTML = "Start";' . "\n";
        echo 'document.querySelector("#banklink_btn").click();}' . "\n";
        echo '}, 1000);' . "\n";
        echo '</script>' . "\n";
    }

    /*
      public function email_instructions( $order, $sent_to_admin, $plain_text ) {
      if ( $plain_text == false && (int) $order->id > 0) {
      echo 'test';
      }
      }
     */

    public function process_payment($order_id) {

        global $woocommerce;

        $order = new WC_Order((int) $order_id);
        //$order->update_status('on-hold', __( 'Awaiting liisi payment', 'liisi' ));

        $woocommerce->cart->empty_cart();

        return array(
            'result' => 'success',
            'redirect' => $order->get_checkout_payment_url(true),
        );
    }

    ##################### DEBUG UTILITY########################
//    public function webhook_debug()
//    {
//        $result[] = getimagesize("http://wordpress.t.vptest.ee/wp-content/uploads/liisilogo/logo.png");
//        $result[] = getimagesize("https://via.placeholder.com/300.png/09f/fff");
//
//        echo '<pre>';
//        echo PHP_EOL.'########### DEBUG RESULT ############'.PHP_EOL;
//        var_dump($result);
//        echo '</pre>';
//        die();
//    }
    ###########################################################
}
